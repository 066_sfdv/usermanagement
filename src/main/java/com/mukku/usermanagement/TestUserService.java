/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.usermanagement;

/**
 *
 * @author acer
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.getUsers());
        //test add user
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUsers());
        //test index out of range
        User user0 = UserService.getUser(4);
        System.out.println(user0);
        //test update user
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("0805");
        System.out.println(UserService.getUsers());
        
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
        System.out.println(UserService.login("admin", "0805_wyb"));
    }
}
